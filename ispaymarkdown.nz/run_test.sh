rm -f *.png
rm -f *.jpg

export TEST_DATE=$(date +%Y-%m-%d_%H_%M_%S)
export DAY_DATE=$(date +%Y-%m-%d)

update_dash() {
  # update component
  echo $1 $2
  curl -L -X PUT --insecure \
       -H "Content-Type: application/json;" \
       -H "X-Cachet-Token: FF2evKBBxKuocMfHfYxp" \
       -d "{\"status\":$2}" \
   https://www.ispaymarkdown.nz/api/v1/components/$1
}

create_incident() {
  # create incident
  curl -L -X POST --insecure -H "Content-Type: application/json;" \
  -H "X-Cachet-Token: FF2evKBBxKuocMfHfYxp" \
  -d "{\"name\":\"Paymark card registration failed\",\"message\":\"$1\",\"status\":1,\"visible\":true,component_id:1,component_status:3}" \
   https://www.ispaymarkdown.nz/api/v1/incidents
}

export NVM_DIR="/var/lib/jenkins/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" >/dev/null  # This loads nvm

export PATH=$PATH:$(pwd)/node_modules/phantomjs/bin

npm install

echo $TEST_DATE
touch output.txt

tail -f output.txt &
TAIL_PID=$!
phantomjs --ssl-protocol=any --ignore-ssl-errors=true ./paymark.js > output.txt

kill $TAIL_PID || true

EXIT_CODE=$?
export CAUSE_OF_FAILURE="Success"
if grep 'load finished:  fail' output.txt > /dev/null; then
  CAUSE_OF_FAILURE="Failed to load page"
  EXIT_CODE=4
fi
if grep 'FAILED: step 1' output.txt > /dev/null; then
  CAUSE_OF_FAILURE="Failed to register card"
  EXIT_CODE=3
fi
if grep 'FAILED: step 3' output.txt > /dev/null; then
  CAUSE_OF_FAILURE="Failed to register card"
  EXIT_CODE=3
fi
if grep 'FAILED: step 3' output.txt > /dev/null; then
  CAUSE_OF_FAILURE="Failed to register card"
  EXIT_CODE=3
fi
count=`ls -1 *.png 2>/dev/null | wc -l`
if [ $count -ne 0 ]; then
  mkdir -p ../assets/$TEST_DATE
  cp *.png ../assets/$TEST_DATE/
fi

echo $CAUSE_OF_FAILURE > status.txt

filename=${TEST_DATE}_failure_$RC.markdown
cat templates/jekyll-template.markdown | envsubst > ../_posts/$filename
cd ..
rm -rf public/*
jekyll build -d public

exit $EXIT_CODE
