var page = require('webpage').create();
var generator = require('creditcard-generator');
var system = require('system');

var cc = generator.GenCC()[0];
var name = 'Tester McTesting';
var exp = '12 / 18';
var startUrl = "https://choyce.org/dashboard/monitor";
var testindex = 0;
var loadInProgress = false;
var exitCode = 0;

console.log('cc: ', cc);

console.log(page.resourceTimeout(60 * 1000));

page.viewportSize = {
  width: 1024,
  height: 768
};

page.onConsoleMessage = function(msg) {
  if (msg.indexOf('ReferenceError') != -1) {
    var fs = require('fs');

    var path = 'status.txt';
    var content = 'Paymark failed to load jquery.';
    fs.write(path, content, 'w');

    exitCode = 42;
  }
  console.log(msg);
};

// page.onResourceError = function(resourceError) {
//     console.error(resourceError.url + ': ' + resourceError.errorString);
// };
//
// page.onResourceRequested = function (request) {
//     system.stderr.writeLine('= onResourceRequested()');
//     system.stderr.writeLine('  request: ' + JSON.stringify(request, undefined, 4));
// };

page.onResourceReceived = function(response) {
  if (response.url.indexOf('static')) {
    return;
  }
  console.log('URL: ' + response.url)
  console.log('Response code: ' + response.status);
  switch (response.status) {
    case null:
      exitCode = 1;
      break;
    case 504:
      exitCode = 504;
      break;
    case 503:
      exitCode = 503;
      break;
  }
  console.log('exitCode: ' + exitCode);

};

page.onLoadStarted = function(status) {
  loadInProgress = true;
  console.log("load started: ", status);
};

page.onLoadFinished = function(status) {
  console.log("load finished: ", status);
  console.log("waiting 15 seconds...");
  setTimeout(function() {
//    page.render(testindex + "screenshot.png");
    loadInProgress = false;
    console.log("done waiting.");
  }, 20000);
};

var steps = [
  function() {
    //Load Login Page
    page.open(startUrl);
  },
  function() {
    //Enter Credentials
    rc = page.evaluate(function(cc, exp, name) {
      $("#cardNumber").val(cc);
      $("#expiryDate").val(exp);
      $("#nameOnCard").val(name);
    }, cc, exp, name);
    console.log('rc', rc);
  },
  function() {
    //Login

    page.render('01form_filled.png');
    page.evaluate(function() {
      $(":button").click();
    });
  },
  function() {
    // Output content of page to stdout after form has been submitted
    page.render('02form_clicked.jpg');

    page.evaluate(function() {
      console.log('done');
    });
  }
];

console.log('starting loop')
interval2 = setInterval(function() {
  if (0 != exitCode) {
    console.log("FAILED: step " + testindex);
    //return phantom.exit(exitCode);
  }
  if (!loadInProgress && typeof steps[testindex] == "function") {
    console.log("step " + (testindex + 1));
    steps[testindex]();
    testindex++;
  }
  if (typeof steps[testindex] != "function") {
    page.render('test_complete.jpg');
    console.log("test complete!");
    phantom.exit(exitCode);
  }
}, 1000);
